### Travis CI - Android Upload

 1. Create keystore
 1. Upload encrypted keystore to repo ([docs](https://docs.travis-ci.com/user/encrypting-files/))
    - `travis encrypt-file --add <KEYSTORE>`
 1. Set secure env variable `STORE_PASS` in travis
 1. Create personal access token for deployment on [github](https://github.com/settings/tokens) (public_repo)
 1. Add encrypted token to deploy section ([docs](https://docs.travis-ci.com/user/deployment-v2/providers/releases/))
    - `travis encrypt --add deploy.token <TOKEN>`

#### Notes
 - When copying files to be used in different project. You have to add executable permission to travis scripts: `git update-index --chmod=+x travis/*`
 - When getting error `Cannot read property 'kind' of undefined`, current fix: `npm i --save-dev @angular-devkit/architect@0.803.20 @angular-devkit/build-angular@0.803.20 @angular-devkit/core@8.3.20 @angular-devkit/schematics@8.3.20`